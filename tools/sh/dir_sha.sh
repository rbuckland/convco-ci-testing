#!/bin/sh

the_dir=$1
find ${the_dir} -type f -exec sha256sum {} \; | sha256sum | cut -c1-16
